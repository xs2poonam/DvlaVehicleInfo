package com.digitalskills.junit;

import com.digitalskills.FileInfo;
import com.digitalskills.FolderReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class FileInfoTest {
    private static final String folderPath="/Users/jitendersingh/PoonamTesting";

    @Test
    public void testGetListOfFileInfoObjects() {
        FolderReader folderReader = new FolderReader();
        List<FileInfo> fileInfoList = folderReader.getFileInfo(folderPath);
        System.out.println(fileInfoList.size());
        Assert.assertFalse("No files found matching filter", fileInfoList.isEmpty());
    }


    @Test(expected = RuntimeException.class)
    public void testInvalidPathThrowsRunTimeException() {
        String invalidFolderPath= folderPath + "/InvalidFolder";
        FolderReader folderReader = new FolderReader();
        folderReader.getFileInfo(invalidFolderPath);
    }
}
