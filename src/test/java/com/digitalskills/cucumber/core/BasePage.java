package com.digitalskills.cucumber.core;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BasePage {
    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private static final int WEBDRIVER_TIMEOUT = 20;

    public BasePage() {
        this.webDriver=WebDriverHelper.getWebDriver();
        this.webDriverWait=new WebDriverWait(webDriver,WEBDRIVER_TIMEOUT);
    }

    protected void gotoUrl(String url) {
        webDriver.manage().deleteAllCookies();
        webDriver.get(url);
    }

    private WebElement getClickableElement(By by) {
        return webDriverWait.withTimeout(WEBDRIVER_TIMEOUT, TimeUnit.SECONDS).until(ExpectedConditions.elementToBeClickable(by));
    }

    protected void click(By by) {
        getClickableElement(by).click();
    }

    protected void sendKeys(By by, String txtToType) {
        getClickableElement(by).sendKeys(txtToType);
    }

    protected String getText(By by) {
        return webDriver.findElement(by).getText();
    }

    private WebElement retryToFind(By by) {
        int attempts = 0;
        while(attempts < 10) {
            try {
                return webDriver.findElement(by);
            } catch(StaleElementReferenceException e) {}
            attempts++;
        }
        throw new RuntimeException("Tried to deal with StaleElement but failed");
    }

    protected void waitForPageToLoad() {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        webDriverWait.until(pageLoadCondition);
    }

}
