package com.digitalskills.cucumber.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverHelper {
    private static WebDriver webDriver;

    private static final Thread CLOSE_THREAD = new Thread() {
        @Override
        public void run() {
            webDriver.quit();
        }
    };

    public static WebDriver getWebDriver() {
        if (webDriver == null) {
            webDriver = new ChromeDriver();
            Runtime.getRuntime().addShutdownHook(CLOSE_THREAD);
        }
        return webDriver;
    }
}
