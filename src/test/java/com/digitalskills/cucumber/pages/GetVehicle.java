package com.digitalskills.cucumber.pages;

import com.digitalskills.cucumber.core.BasePage;
import org.openqa.selenium.By;

public class GetVehicle extends BasePage {
    private By btnStartNow = By.cssSelector("a[href*='vehicleenquiry.service.gov.uk']");

    private String dvlaUrl = "https://www.gov.uk/get-vehicle-information-from-dvla";

    public void launchDvlaVehicleInfo() {
        gotoUrl(dvlaUrl);
    }

    public void startLookingForVehicle() {
        click(btnStartNow);
    }

}
