package com.digitalskills.cucumber.pages;

import com.digitalskills.cucumber.core.BasePage;
import org.openqa.selenium.By;

public class VehicleEnquiry extends BasePage {

    private By txtRegistrationNumber = By.id("Vrm");
    private By btnContinue = By.name("Continue");

    public void searchVehicle(String registrationNumber) {
        sendKeys(txtRegistrationNumber, registrationNumber);
        click(btnContinue);
    }


}
