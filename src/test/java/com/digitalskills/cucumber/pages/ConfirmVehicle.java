package com.digitalskills.cucumber.pages;

import com.digitalskills.cucumber.core.BasePage;
import com.digitalskills.cucumber.domain.Car;
import org.openqa.selenium.By;

public class ConfirmVehicle extends BasePage {
    private By lblRegistrationNumber = By.cssSelector("ul.list-summary li:nth-child(1) span:nth-child(2)");
    private By lblColour = By.cssSelector("ul.list-summary li:nth-child(2) span:nth-child(2)");
    private By lblMake = By.cssSelector("ul.list-summary li:nth-child(3) span:nth-child(2)");
    private By lnkBack = By.cssSelector("a.link-back");

    public Car getActualCarInfo() {
        waitForPageToLoad();
        String registrationNumber = getText(lblRegistrationNumber);
        String colour = getText(lblColour);
        String make = getText(lblMake);
        return new Car(registrationNumber, colour, make);
    }

    public void getBackToGetVehicle() {
        click(lnkBack);
    }
}
