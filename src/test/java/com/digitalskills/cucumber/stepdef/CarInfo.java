package com.digitalskills.cucumber.stepdef;

import com.digitalskills.cucumber.domain.Car;
import com.digitalskills.cucumber.pages.ConfirmVehicle;
import com.digitalskills.cucumber.pages.GetVehicle;
import com.digitalskills.cucumber.pages.VehicleEnquiry;
import com.digitalskills.data.reader.CarInfoReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.asserts.SoftAssert;
import java.util.ArrayList;
import java.util.List;

public class CarInfo {

    private GetVehicle getVehicle;
    private ConfirmVehicle confirmVehicle;
    private VehicleEnquiry vehicleEnquiry;
    List<Car> expectedCars = new ArrayList<>();
    List<Car> actualCars = new ArrayList<>();

    public CarInfo(VehicleEnquiry vehicleEnquiry, ConfirmVehicle confirmVehicle, GetVehicle getVehicle) {
        this.getVehicle = getVehicle;
        this.confirmVehicle = confirmVehicle;
        this.vehicleEnquiry = vehicleEnquiry;
    }

    @Given("^user open DLVA vehicle information page$")
    public void userOpenDLVAVehicleInformationPage() {
        getVehicle.launchDvlaVehicleInfo();
    }

    @When("^query is made for vehicle registration numbers$")
    public void queryIsMadeForVehicleRegistrationNumbers() {
        expectedCars = CarInfoReader.readCarInfo();
        getVehicle.startLookingForVehicle();
        for (Car expectedCar : expectedCars) {
            vehicleEnquiry.searchVehicle(expectedCar.getRegistrationNumber());
            actualCars.add(confirmVehicle.getActualCarInfo());
            confirmVehicle.getBackToGetVehicle();
        }
    }

    @Then("^user should see corresponding vehicle details$")
    public void userShouldSeeCorrespondingVehicleDetails() {
        SoftAssert softAssert = new SoftAssert();
        for (int i = 0; i < expectedCars.size(); i++) {
            softAssert.assertEquals(expectedCars.get(i).getVehicleColour().toLowerCase()
                    , actualCars.get(i).getVehicleColour().toLowerCase()
                    , "Verifying registration Number: " + expectedCars.get(i).getRegistrationNumber());

            softAssert.assertEquals(expectedCars.get(i).getVehicleMake().toLowerCase()
                    , actualCars.get(i).getVehicleMake().toLowerCase()
                    , "Verifying registration Number: " + expectedCars.get(i).getRegistrationNumber());
        }
        softAssert.assertAll();
    }

}
