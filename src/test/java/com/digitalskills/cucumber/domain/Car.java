package com.digitalskills.cucumber.domain;

public class Car {
    private String registrationNumber;
    private String vehicleMake;
    private String vehicleColour;

    public Car(String registrationNumber, String vehicleMake, String vehicleColour) {
        this.registrationNumber = registrationNumber;
        this.vehicleMake = vehicleMake;
        this.vehicleColour = vehicleColour;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleColour() {
        return vehicleColour;
    }

    public void setVehicleColour(String vehicleColour) {
        this.vehicleColour = vehicleColour;
    }

}
