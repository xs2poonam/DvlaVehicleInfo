package com.digitalskills.data.reader;

import com.digitalskills.FileInfo;
import com.digitalskills.FolderReader;
import com.digitalskills.cucumber.domain.Car;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CarInfoReader {
    private static String path = System.getProperty("user.dir") + "/folderWithDataFiles/" ;
    static {
        if(! (System.getProperty("pathToDataFiles")==null) ) {
            path = System.getProperty("pathToDataFiles");
        }
    }

    public static List<Car> readCarInfo() {
        FolderReader folderReader = new FolderReader();
        List<FileInfo> fileInfoList = folderReader.getFileInfo(path);
        return buildCarInfo(fileInfoList);
    }

    private static List<Car> buildCarInfo(List<FileInfo> fileInfoList) {
        List<Car> cars = new ArrayList<>();
        for (FileInfo fileInfo : fileInfoList) {
            if (fileInfo.getExtension().equalsIgnoreCase("xls") || fileInfo.getExtension().equalsIgnoreCase("xlsx")) {
                cars.addAll(getDataFromExcelFile(fileInfo.getFileName()));
            } else {
                cars.addAll(getDataFromCsvFile(fileInfo.getFileName()));
            }
        }
        return cars;
    }

    private static List<Car> getDataFromExcelFile(String fileName) {
        FileInputStream file;
        Workbook workbook = null;
        List<Car> cars = new ArrayList<>();
        try {
            file = new FileInputStream(new File(path + fileName));
            workbook = WorkbookFactory.create(file);
        } catch (InvalidFormatException | IOException e) {
            e.printStackTrace();
        }
        Sheet dataTypeSheet = workbook.getSheetAt(0);

        for (int i = 1; i <= dataTypeSheet.getLastRowNum(); i++) {
            Iterator<Cell> cellIterator = dataTypeSheet.getRow(i).iterator();
            String registrationNumber = cellIterator.next().getStringCellValue();
            String colour = cellIterator.next().getStringCellValue();
            String make = cellIterator.next().getStringCellValue();
            cars.add(new Car(registrationNumber, make, colour));
        }
        return cars;
    }

    private static List<Car> getDataFromCsvFile(String fileName) {
        List<Car> cars = new ArrayList<>();
        String cvsSplitBy = ",";
        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(path + fileName))) {
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] carData = line.split(cvsSplitBy);
                String registrationNumber = carData[0];
                String colour = carData[1];
                String make = carData[2];
                cars.add(new Car(registrationNumber, make, colour ));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cars;
    }
}
