Feature: Verify Car details

  @exercise
  Scenario: Verify car details on DLVA are shown correctly
    Given user open DLVA vehicle information page
    When query is made for vehicle registration numbers
    Then user should see corresponding vehicle details