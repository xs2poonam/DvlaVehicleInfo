package com.digitalskills;

public class FileInfo {
    private String fileName;
    private Long size;
    private String mimeType;
    private String extension;


    public FileInfo(String fileName, Long size, String mimeType, String extension) {
        this.fileName = fileName;
        this.size = size;
        this.mimeType = mimeType;
        this.extension = extension;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
