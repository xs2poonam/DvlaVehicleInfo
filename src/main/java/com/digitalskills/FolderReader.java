package com.digitalskills;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class FolderReader {

    public List<FileInfo> getFileInfo(String path) {
        List<FileInfo> fileInfoList = new LinkedList<>();
        for (SupportedMimeType supportedMimeType : SupportedMimeType.values()) {
            FileFilter fileFilter = getFileFilter(supportedMimeType);
            File[] listOfFiles = getAllFiles(path, fileFilter);
            if(listOfFiles==null) {
                throw new RuntimeException("Invalid path: " + path);
            }
            fileInfoList.addAll(extractFileInfo(listOfFiles));
        }
        return fileInfoList;
    }

    private File[] getAllFiles(String path, FileFilter fileFilter) {
        return new File(path).listFiles(fileFilter);
    }

    private WildcardFileFilter getFileFilter(SupportedMimeType supportedMimeType) {
        return new WildcardFileFilter("*." + supportedMimeType.name());
    }

    private List<FileInfo> extractFileInfo(File[] listOfFiles) {
        List<FileInfo> fileInfoList = new LinkedList<>();
        for (File file : listOfFiles) {
            String fileName = file.getName();
            if (file.isFile() && !(FilenameUtils.getBaseName(fileName).isEmpty())) {
                String extension = FilenameUtils.getExtension(fileName);
                long size = FileUtils.sizeOf(file);
                String mimeType = getMimeType(fileName);
                fileInfoList.add(new FileInfo(fileName, size, mimeType, extension));
            }
        }
        return fileInfoList;
    }

    private String getMimeType(String fileName) {
        try {
            return Files.probeContentType(Paths.get(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("File " + fileName + "doesn't exist");
        }
    }
}