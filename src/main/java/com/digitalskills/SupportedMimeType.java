package com.digitalskills;

public enum SupportedMimeType {
    xlsx,
    xls,
    csv;
}
